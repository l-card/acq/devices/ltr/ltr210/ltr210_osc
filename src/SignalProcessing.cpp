#include "SignalProcessing.h"

#include "lmath.h"

#include <math.h>

SignalProcessing::SignalProcessing(QObject *parent) : QObject(parent)
{
}


int SignalProcessing::processFrame(LQChFrameParams ch1, LQChFrameParams ch2,
                                   TLTR210_FRAME_STATUS status, int proc_flags)
{
    QLtr210 *dev = qobject_cast<QLtr210 *>(sender());
    if (dev)
    {

        if (ch1.size)
            processChData(dev, &ch1, proc_flags);
        if (ch2.size)
            processChData(dev, &ch2, proc_flags);

        emit frameProcessed(dev, ch1, ch2, status);
    }

    return 0;
}

int SignalProcessing::processChData(QLtr210* dev, LQChFrameParams *ch, int proc_flags)
{
    t_lmath_win_params win_par;
    int i;
    int err = 0;
    ch->range_val = proc_flags & LTR210_PROC_FLAG_VOLT ? f_range_vals[dev->config().Ch[ch->ch].Range] : LTR210_ADC_SCALE_CODE_MAX;
    ch->fft_size = ch->size/2+1;


    ch->dt = 1./dev->adcFreq();


    err = lmath_acdc_estimation(ch->data, ch->size, &ch->avg, &ch->rms);

    if (!err && (ch->size > 16))
    {
        double *scaled_vals = new double[ch->size];
        ch->fft = new double[ch->size/2+1];
        /* накладываем окно */

        if (!err)
            err = lmath_scaled_window(ch->data, ch->size, LMATH_WINTYPE_BH_4TERM, 0, &win_par, scaled_vals);
        /* вычисляем амплитудный спектр */
        if (!err)
            err = lmath_amp_pha_spectrum(scaled_vals, ch->size, ch->dt, 0, ch->fft, NULL, &ch->df);

        /* ищем максимальную частоту */
        if (!err)
        {
            err = lmath_find_peak_freq_spectrum(ch->fft, ch->fft_size, ch->df, 0,
                                                win_par, &ch->f_max, 0);
        }

        if (!err)
        {
            lmath_calc_spectrum_params(ch->fft, ch->fft_size, ch->df,
                                       ch->f_max, 7, win_par,
                                       &ch->snr, &ch->thd, &ch->sinad,
                                       &ch->sfdr, &ch->enob);
        }

        if (!err)
        {
            /* перевод fft в логарфмический масштаб */
            for(i=0; i < ch->fft_size; i++)
            {
                ch->fft[i] = 20.0*log10(ch->fft[i]/(ch->range_val/sqrt(2.)));
            }
        }
        delete scaled_vals;
    }

    return err;
}
