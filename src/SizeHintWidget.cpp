#include "SizeHintWidget.h"

SizeHintWidget::SizeHintWidget(QSize size, QWidget *parent) :
    QWidget(parent), m_prefSize(size)
{
    setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
}

QSize SizeHintWidget::sizeHint() const
{
    return m_prefSize;
}
