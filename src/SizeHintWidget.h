#ifndef SIZEHINTWIDGET_H
#define SIZEHINTWIDGET_H

#include <QWidget>

class SizeHintWidget : public QWidget
{
    Q_OBJECT
public:
    explicit SizeHintWidget(QSize size, QWidget *parent = 0);
    
    virtual QSize sizeHint () const;
signals:
    
public slots:
    
private:
    QSize m_prefSize;
};

#endif // SIZEHINTWDGET_H
