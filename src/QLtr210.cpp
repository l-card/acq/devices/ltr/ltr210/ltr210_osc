#include "QLtr210.h"

#include <QCoreApplication>
#include <QxtLogger>
#include <QString>
#include <math.h>
#include <stdio.h>


#ifdef Q_OS_WIN
    #define QSTRING_FROM_CSTR(str) QString::fromLocal8Bit(str)
#else
    #define QSTRING_FROM_CSTR(str) QString(str)
#endif


#define OSC_ERROR_TEST_CNTR                 -65535
#define OSC_ERROR_KEEPALIVE_TIMEOUT         -65534
#define OSC_ERROR_INVALID_FRAME_SIZE        -65533

#define OSC_KEEPALIVE_TIMEOUT               10000




QLtr210::QLtr210(int slot, QString csn, QString ctype, QObject *parent) :
    QThread(parent), m_csn(csn), m_slot(slot+LTR_CC_CHNUM_MODULE1), m_crate_type(ctype), m_open(false), m_codes(false),
    m_zero_cor(false), m_fpga_is_loaded(false)
{
    LTR210_Init(&m_hnd);
}

int QLtr210::open()
{
    int err =  LTR_OK;
    if (!m_open)
        err = LTR210_Open(&m_hnd, LTRD_ADDR_DEFAULT, LTRD_PORT_DEFAULT, m_csn.toAscii(), m_slot);
    if (err==LTR_OK)
        m_open = true;
    else
        emit error(tr("Ошибка открытия модуля"), err);

    if (err==LTR_OK)
    {
        m_fpga_is_loaded = LTR210_FPGAIsLoaded(&m_hnd)==LTR_OK;
    }
    emit devDescrChanged(devDescr());

    return err;
}

int QLtr210::close()
{
    INT err = LTR210_Close(&m_hnd);
    if (err==LTR_OK)
        m_open = false;
    return err;
}

int QLtr210::setConfig(const TLTR210_CONFIG *cfg)
{
    INT err = LTR_OK;
    if (!m_open)
        err = open();

    if ((err==LTR_OK) && (LTR210_FPGAIsLoaded(&m_hnd)!=LTR_OK))
    {
        err = loadFPGA("");
    }

    if (err==LTR_OK)
    {
        m_hnd.Cfg = *cfg;
        err = LTR210_SetADC(&m_hnd);
        if (err!=LTR_OK)
        {
            reportError(tr("Ошибка установки настроек АЦП"), err);
        }
        else
        {
            reportMessage(QxtLogger::InfoLevel, "Установлены настройки модуля");
        }
    }

    return err;
}

int QLtr210::loadFPGA(QString filename)
{
    INT err = LTR_OK;
    if (!m_open)
        err = open();
    if (err==LTR_OK)
        reportMessage(QxtLogger::InfoLevel, tr("Начата загрузка ПЛИС"));
    if (err==LTR_OK)
        err = LTR210_LoadFPGA(&m_hnd, filename.toAscii(), cb_load_porgr, this);


    emit loadFpgaStop(err);
    QCoreApplication::flush();
    QCoreApplication::processEvents();



    if (err!=LTR_OK)
    {
        reportError(tr("Ошибка загрузки прошивки ПЛИС"), err);
    }
    else
    {
        reportMessage(QxtLogger::InfoLevel, tr("Загрузка ПЛИС завершена"));
        m_fpga_is_loaded = true;
        emit devDescrChanged(devDescr());
    }
    return err;
}

void QLtr210::stopRequest()
{
    m_stop_req = true;
}

int QLtr210::startFrame()
{
    return LTR210_FrameStart(&m_hnd);
}

int QLtr210::startADC()
{
    int err = LTR210_Start(&m_hnd);
    if (err==LTR_OK)
    {
        reportMessage(QxtLogger::InfoLevel, tr("Запущен сбор данных"));
    }
    else
    {
        reportError(tr("Ошибка запуска модуля"), err);
    }
    return err;
}

int QLtr210::measZero()
{
    int err = LTR210_MeasAdcZeroOffset(&m_hnd, 0);
    if (err==LTR_OK)
    {
        reportMessage(QxtLogger::InfoLevel, tr("Успешно завершено измерение собственного нуля"));
    }
    else
    {
        reportError(tr("Ошибка измерения собственного нуля"), err);
    }
    return err;
}

QString QLtr210::devDescr() const
{
    QString str = devStr() + ", верcии прош.: PLD = "
            + QString::number(m_hnd.ModuleInfo.VerPLD);
    if (m_fpga_is_loaded)
    {
        str = str.append(", ПЛИС = " + QString::number(m_hnd.ModuleInfo.VerFPGA));
    }
    return str;
}

int QLtr210::processRecvdFrame(DWORD* wrds, double *data, TLTR210_DATA_INFO *info, int size,
                                int proc_flags, TLTR210_FRAME_STATUS status)
{
    int err = LTR_OK;
    LQChFrameParams par[LTR210_CHANNEL_CNT];

    memset(&par[0], 0, sizeof(LQChFrameParams)*LTR210_CHANNEL_CNT);
    for (unsigned ch=0; ch < LTR210_CHANNEL_CNT; ch++)
    {
        par[ch].ch = ch;
    }

    if (status.Result==LTR210_FRAME_RESULT_OK)
    {
        /* В тестовом режиме нам приходит значения
         * счетчика по модулю 63949. Проверяем его
         * непрерывность */
        if (m_hnd.Cfg.Flags & LTR210_CFG_FLAGS_TEST_CNTR_MODE)
        {
            WORD cntr = wrds[0] >> 16;
            INT cntr_err = 0;

            for (int i=1; i < size; i++)
            {
                WORD new_cntr = wrds[i] >> 16;
                if (++cntr==63949)
                    cntr=0;
                if (new_cntr!=cntr)
                {
                    cntr_err++;
                    cntr = new_cntr;
                }
            }

            if (cntr_err)
            {
                err = OSC_ERROR_TEST_CNTR;
            }
        }

        if (m_hnd.Cfg.Flags & LTR210_CFG_FLAGS_TEST_CNTR_MODE)
        {
            par[0].data = new double[2*m_hnd.Cfg.FrameSize];
            par[0].din = new double[2*m_hnd.Cfg.FrameSize];
            par[1].data = 0;
        }
        else
        {
            for (unsigned ch=0; ch < LTR210_CHANNEL_CNT; ch++)
            {
                par[ch].data = new double[m_hnd.Cfg.FrameSize];
                par[ch].din = new double[m_hnd.Cfg.FrameSize];
            }
        }

        for (int pt=0; pt < size; pt++)
        {
            int ch = info[pt].Ch;
            par[ch].data[par[ch].size] = data[pt];
            par[ch].din[par[ch].size] = info[pt].DigBitState & 1;
#if 0
            if ((par[ch].size) && (par[ch].din[par[ch].size-1]!=par[ch].din[par[ch].size]))
            {
                reportMessage(QxtLogger::DebugLevel, QString("Канал %1: Изменение уровня Sync на отсчете %2 c %3 на %4").arg(ch+1).arg(par[ch].size+1).
                              arg(int(par[ch].din[par[ch].size-1])).arg(int(par[ch].din[par[ch].size])));
            }
#endif
            par[ch].size++;
        }
    }

    emit frameRecvd(par[0], par[1], status, proc_flags);

    return err;
}

void QLtr210::run()
{
    QString errTitle = tr("Ошибка приема данных");
    INT err=LTR_OK;
    m_stop_req = false;


    INT stop_err;
    DWORD *wrds = new DWORD[m_hnd.State.RecvFrameSize];
    double *proc_wrd = new double[m_hnd.State.RecvFrameSize];
    TLTR210_DATA_INFO* info = new TLTR210_DATA_INFO[m_hnd.State.RecvFrameSize];



    while ((err==LTR_OK) && !m_stop_req)
    {
        INT recv_cnt, rd_pos;
        DWORD evt;

        if ((m_hnd.Cfg.SyncMode == LTR210_SYNC_MODE_CONTINUOUS)
                && (m_hnd.Cfg.GroupMode != LTR210_GROUP_MODE_SLAVE))
        {
            rd_pos= 0;
            while ((rd_pos!=m_hnd.State.RecvFrameSize) && (err==LTR_OK) && !m_stop_req)
            {
                errTitle = tr("Ошибка приема данных");
                recv_cnt = LTR210_Recv(&m_hnd, &wrds[rd_pos], NULL, m_hnd.State.RecvFrameSize-rd_pos, 200);
                if (recv_cnt<0)
                    err = recv_cnt;
                else
                {
                    rd_pos+=recv_cnt;
                }

                if (rd_pos==m_hnd.State.RecvFrameSize)
                {                    
                    TLTR210_FRAME_STATUS frame_st;
                    int proc_flags = m_codes ? 0 : LTR210_PROC_FLAG_VOLT;
                    if (m_afc_cor)
                        proc_flags |= LTR210_PROC_FLAG_AFC_COR;
                    if (m_zero_cor)
                        proc_flags |= LTR210_PROC_FLAG_ZERO_OFFS_COR;

                    recv_cnt = rd_pos;
                    errTitle = tr("Ошибка обработки данных");
                    err = LTR210_ProcessData(&m_hnd, wrds, proc_wrd, &recv_cnt,
                                             proc_flags,
                                             NULL, info);


                    if (err==LTR_OK)
                    {                                           
                        frame_st.Result = LTR210_FRAME_RESULT_OK;
                        frame_st.Reserved = 0;
                        frame_st.Flags = 0;

                        err = processRecvdFrame(wrds, proc_wrd, info, recv_cnt, proc_flags, frame_st);
                    }
                }
            }
        }
        else
        {
            DWORD per_st;
            err = LTR210_WaitEvent(&m_hnd, &evt, &per_st, 500);
            if (err==LTR_OK)
            {
                switch (evt)
                {
                    case LTR210_RECV_EVENT_SOF:
                        rd_pos= 0;
                        while ((rd_pos!=m_hnd.State.RecvFrameSize) && (err==LTR_OK) && !m_stop_req)
                        {
                            errTitle = tr("Ошибка приема данных");
                            recv_cnt = LTR210_Recv(&m_hnd, &wrds[rd_pos], NULL, m_hnd.State.RecvFrameSize-rd_pos, 200);
                            if (recv_cnt<0)
                                err = recv_cnt;
                            else
                            {
                                rd_pos+=recv_cnt;
                            }

                            if (rd_pos==m_hnd.State.RecvFrameSize)
                            {                                
                                TLTR210_FRAME_STATUS frame_st;
                                int proc_flags = m_codes ? 0 : LTR210_PROC_FLAG_VOLT;
                                if (m_afc_cor)
                                    proc_flags |= LTR210_PROC_FLAG_AFC_COR;
                                if (m_zero_cor)
                                    proc_flags |= LTR210_PROC_FLAG_ZERO_OFFS_COR;

                                recv_cnt = rd_pos;
                                errTitle = tr("Ошибка обработки данных");
                                err = LTR210_ProcessData(&m_hnd, wrds, proc_wrd, &recv_cnt,
                                                         proc_flags,
                                                         &frame_st, info);

                                /* размер должен соответствовать принятому без слова статуса */
                                if ((err==LTR_OK) && (recv_cnt!=(m_hnd.State.RecvFrameSize-1)))
                                {
                                    err = OSC_ERROR_INVALID_FRAME_SIZE;
                                }


                                if (err==LTR_OK)
                                {
                                    if (frame_st.Result!=LTR210_FRAME_RESULT_PENDING)
                                    {
                                        err = processRecvdFrame(wrds, proc_wrd, info, recv_cnt, proc_flags, frame_st);
                                    }
                                    else
                                    {
                                        /* если не нашили конец кадра, то в данном случае это
                                         * ошибка */
                                        err = OSC_ERROR_INVALID_FRAME_SIZE;
                                    }
                                }

                            }
                        }
                        break;
                    default:
                        /* при разрешенных периодических статусах, если не приняли
                         * ничего за заданное время, то выходим по ошибке */
                        if (m_hnd.Cfg.Flags & LTR210_CFG_FLAGS_KEEPALIVE_EN)
                        {
                            DWORD interval;
                            errTitle = tr("Ошибка приема данных");
                            err = LTR210_GetLastWordInterval(&m_hnd, &interval);
                            if ((err==LTR_OK) && (interval > OSC_KEEPALIVE_TIMEOUT))
                            {
                                err = OSC_ERROR_KEEPALIVE_TIMEOUT;
                            }
                        }
                        break;
                }
            }
        }
    }

    delete [] wrds;
    delete [] proc_wrd;
    delete [] info;



    stop_err = LTR210_Stop(&m_hnd);
    if (err==LTR_OK)
    {
        errTitle = tr("Ошибка останова модуля");
        err = stop_err;

        reportMessage(QxtLogger::InfoLevel, tr("Сбор данных остановлен"));
    }

    if (err)
        reportError(errTitle, err);
}


QList<QLtr210 *> QLtr210::deviceList()
{
    QList<QLtr210 *> devlist;
    TLTR srv;
    INT err;

    LTR_Init(&srv);
    srv.cc = LTR_CC_CHNUM_CONTROL;
    strcpy(srv.csn, LTR_CSN_SERVER_CONTROL);

    err = LTR_Open(&srv);
    if (!err)
    {
        char serials[LTR_CRATES_MAX][LTR_CRATE_SERIAL_SIZE];
        err = LTR_GetCrates(&srv, (BYTE*)serials);
        if (!err)
        {
            int i;
            for (i=0; i < LTR_CRATES_MAX; i++)
            {
                if (serials[i][0]!='\0')
                {
                    TLTR crate;
                    int crate_err = 0;
                    LTR_Init(&crate);
                    crate.cc = LTR_CC_CHNUM_CONTROL;
                    memcpy(crate.csn, serials[i], LTR_CRATE_SERIAL_SIZE);
                    crate_err = LTR_Open(&crate);
                    if (!crate_err)
                    {
                        TLTR_CRATE_INFO crate_info;
                        WORD mids[LTR_MODULES_PER_CRATE_MAX];
                        crate_err = LTR_GetCrateInfo(&crate, &crate_info);
                        if (!crate_err)
                            crate_err = LTR_GetCrateModules(&crate, mids);
                        if (!crate_err)
                        {
                            QString ctype;
                            switch(crate_info.CrateType)
                            {
                                case LTR_CRATE_TYPE_LTR010:
                                    ctype = "LTR-U-8/16";
                                    break;
                                case LTR_CRATE_TYPE_LTR021:
                                    ctype = "LTR-U-1";
                                    break;
                                case LTR_CRATE_TYPE_LTR030:
                                    ctype = "LTR-EU-8/16";
                                    break;
                                case LTR_CRATE_TYPE_LTR031:
                                    ctype = "LTR-EU-2";
                                    break;
                            }



                            for (int slot=0; slot < LTR_MODULES_PER_CRATE_MAX; slot++)
                            {
                                if (mids[slot]==LTR_MID_LTR210)
                                {
                                    QLtr210* hnd = new QLtr210(slot, QString::fromAscii(serials[i]), ctype);
                                    int err = hnd->open();
                                    if (err==LTR_OK)
                                    {
                                        devlist << hnd;
                                        hnd->close();
                                        hnd->reportMessage(QxtLogger::InfoLevel, "Найден новый модуль");
                                    }
                                    else
                                    {
                                        hnd->close();                                        
                                        hnd->reportError("Не удалось открыть найденный модуль!", err);
                                        delete hnd;
                                    }
                                }
                            }
                        }
                        LTR_Close(&crate);
                    }
                }
            }
        }
        LTR_Close(&srv);
    }

    return devlist;

}

QString QLtr210::errorString(int err)
{
    if (err == OSC_ERROR_TEST_CNTR)
        return tr("Ошибка тестового счетчика");
    if (err == OSC_ERROR_KEEPALIVE_TIMEOUT)
        return tr("Не было статусов от модуля за заданный интервал");
    if (err==OSC_ERROR_INVALID_FRAME_SIZE)
        return tr("Неверный размер принятого кадра!");
    return QSTRING_FROM_CSTR(LTR210_GetErrorString(err));
}


void APIENTRY QLtr210::cb_load_porgr(void *cb, TLTR210* hnd, DWORD done_cnt, DWORD total_cnt)
{
    QLtr210 *dev = (QLtr210*)(cb);
    if (dev)
    {
        if (done_cnt==0)
            dev->sendFpgaStart(total_cnt);
        dev->sendFpgaProgr(done_cnt, total_cnt);
    }
}

void QLtr210::sendFpgaStart(quint32 size)
{
    emit loadFpgaStart(size);
    QCoreApplication::flush();
    QCoreApplication::processEvents();
}


void QLtr210::sendFpgaProgr(quint32 done, quint32 size)
{
    emit loadFpgaProgr(done, size);
    QCoreApplication::flush();
    QCoreApplication::processEvents();
}




void QLtr210::reportError(QString message, int err) const
{
    reportMessage(QxtLogger::ErrorLevel, message+ ":" + errorString(err));
    emit error(message, err);
}

void QLtr210::reportMessage(QxtLogger::LogLevel lvl, QString message) const
{
    qxtLog->log(lvl, QList<QVariant>() << devStr() + ": " + message);
}
