#ifndef QSIGNALPROCESSING_H
#define QSIGNALPROCESSING_H

#include <QObject>

#include "QLtr210.h"



/* Класс для обработки принятого сигнала (вычисление FFT и параметров сигнала).
 * принимает LQChFrameParams с заполненными ch, size и data и заполняет остальные
 * поля, после чего посылает сигнал frameProcessed() */
class SignalProcessing : public QObject
{
    Q_OBJECT
public:
    SignalProcessing(QObject* parent=0);

public slots:
    int processFrame(LQChFrameParams ch1, LQChFrameParams ch2, TLTR210_FRAME_STATUS status, int proc_flags);
    int processChData(QLtr210 *dev, LQChFrameParams *ch, int proc_flags);

signals:
    void frameProcessed(QLtr210 *dev, LQChFrameParams ch1, LQChFrameParams ch2, TLTR210_FRAME_STATUS status);
};

#endif // QSIGNALPROCESSING_H
