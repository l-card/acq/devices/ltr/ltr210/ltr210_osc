#include "SignalParamsPanel.h"
#include "SizeHintWidget.h"
#include "LQTableWidget.h"
#include <QColorDialog>
#include <QHeaderView>
#include <QLocale>

SignalParamsPanel::SignalParamsPanel(QWidget *parent) :
    QDockWidget(parent)
{
    this->setObjectName("SignalParamsPanel");
    setProperty("windowTitle", tr("Параметры сигналов"));

    m_tbl = new LQTableWidget(this);

    QStringList hdrs;
    hdrs << "Модуль" << "Канал" << "Цвет" << "DC" << "AC (RMS)"
         << "Частота" << "SNR" << "THD" << "SINAD" << "SFDR" << "ENOB"
         << tr("Кадров") << "Overlaps" << "Sync skip" << "Inv. Hist";

    m_tbl->setColumnCount(hdrs.size());
    m_tbl->setHorizontalHeaderLabels(hdrs);
    m_tbl->horizontalHeader()->setStretchLastSection(true);
    m_tbl->verticalHeader()->setVisible(false);
    m_tbl->setEditTriggers(QAbstractItemView::NoEditTriggers);

    connect(m_tbl, SIGNAL(itemDoubleClicked(QTableWidgetItem*)), SLOT(on_itemDoubleClicked(QTableWidgetItem*)));
    connect(m_tbl, SIGNAL(itemChanged(QTableWidgetItem*)), SLOT(on_itemChanged(QTableWidgetItem*)));

    this->setWidget(m_tbl);
}

void SignalParamsPanel::setColors(QList<QColor> colors)
{
    m_colors = colors;
}

void SignalParamsPanel::setChEnabled(QList<bool> en)
{
    m_ch_en = en;
}

void SignalParamsPanel::addDevice(QLtr210 *dev)
{
    int row_cnt = m_tbl->rowCount();
    m_tbl->setRowCount(row_cnt+2);

    QTableWidgetItem* item = new QTableWidgetItem(dev->devStr());
    m_tbl->setItem(row_cnt, 0, item);
    m_tbl->setSpan(row_cnt, 0, 2, 1);

    for (int col = 1; col < m_tbl->columnCount(); col++)
    {
        for (int ch=0; ch < LTR210_CHANNEL_CNT; ch++)
        {
            QTableWidgetItem* ch_item=0;
            if (col==column_ch)
            {
                ch_item = new QTableWidgetItem(tr("Канал ") + QString::number(ch+1));
                ch_item->setFlags(ch_item->flags() | Qt::ItemIsUserCheckable | Qt::ItemIsTristate);
                if ((row_cnt+ch) < m_colors.size())
                    ch_item->setCheckState(m_ch_en[row_cnt+ch] ? Qt::Checked : Qt::Unchecked);
                else
                    ch_item->setCheckState(Qt::Checked);
            }
            else if (col==column_color)
            {
                ch_item = new QTableWidgetItem();
                ch_item->setFlags(Qt::ItemIsEnabled);
                if ((row_cnt+ch) < m_colors.size())
                    ch_item->setBackgroundColor(m_colors[row_cnt+ch]);
            }
            else if (col>= column_cntr_first)
            {
                if (ch==0)
                {
                    ch_item = new QTableWidgetItem();
                    ch_item->setText("0");
                    m_tbl->setSpan(row_cnt, col, 2, 1);
                    ch_item->setTextAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
                }
            }
            else
            {
                ch_item = new QTableWidgetItem();
            }

            if (ch_item)
                m_tbl->setItem(row_cnt+ch, col, ch_item);
        }
    }

    m_devNames.append(dev->devStr());
}

void SignalParamsPanel::resetParams()
{
    for (int row =0; row < m_tbl->rowCount(); row++)
    {
        for (int column = column_par_first; column < column_cntr_first; column++)
        {
            m_tbl->item(row, column)->setText("");
        }

        if (!(row%2))
        {
            for (int column = column_cntr_first; column < m_tbl->columnCount(); column++)
            {
                m_tbl->item(row, column)->setText("0");
                m_tbl->item(row, column)->setBackgroundColor(Qt::white);
            }
        }
    }
}

void SignalParamsPanel::clear()
{
    m_tbl->setRowCount(0);
    m_devNames.clear();
}

void SignalParamsPanel::showParams(QLtr210 *dev, int ch, double avg, double rms, double freq, double snr, double thd, double sinad, double sfdr, double enob)
{
    int i = m_devNames.indexOf(dev->devStr());
    if (i>=0)
    {
        QLocale locale;
        int row = i*2+ch;
        int column = column_par_first;


        m_tbl->item(row, column++)->setText(locale.toString(avg, 'f', 6));
        m_tbl->item(row, column++)->setText(locale.toString(rms, 'f', 6));
        m_tbl->item(row, column++)->setText(locale.toString(freq, 'f', 0));
        m_tbl->item(row, column++)->setText(locale.toString(snr, 'f', 2));
        m_tbl->item(row, column++)->setText(locale.toString(thd, 'f', 2));
        m_tbl->item(row, column++)->setText(locale.toString(sinad, 'f', 2));
        m_tbl->item(row, column++)->setText(locale.toString(sfdr, 'f', 2));
        m_tbl->item(row, column++)->setText(locale.toString(enob, 'f', 2));
    }
}

void SignalParamsPanel::clearParams(QLtr210 *dev, int ch)
{
    int i = m_devNames.indexOf(dev->devStr());
    if (i>=0)
    {
        int row = i*2+ch;
        int column = column_par_first;
        m_tbl->item(row, column++)->setText("");
        m_tbl->item(row, column++)->setText("");
        m_tbl->item(row, column++)->setText("");
        m_tbl->item(row, column++)->setText("");
        m_tbl->item(row, column++)->setText("");
        m_tbl->item(row, column++)->setText("");
        m_tbl->item(row, column++)->setText("");
        m_tbl->item(row, column++)->setText("");
    }
}

void SignalParamsPanel::updateFrameCntr(QLtr210 *dev, TLTR210_FRAME_STATUS status)
{
    int i = m_devNames.indexOf(dev->devStr());
    if (i>=0)
    {
        int row = i*2;
        int column = column_cntr_first;
        QLocale locale;
        m_tbl->item(row, column)->setText(locale.toString(m_tbl->item(row, column)->text().toInt()+1));
        //dev->reportMessage(QxtLogger::DebugLevel, tr("Был принят кадр"));
        if (status.Flags & LTR210_STATUS_FLAG_OVERLAP)
        {
            m_tbl->item(row, column_overlap)->setText(locale.toString(m_tbl->item(row, column_overlap)->text().toInt()+1));
            m_tbl->item(row, column_overlap)->setBackgroundColor(Qt::red);
            //dev->reportMessage(QxtLogger::DebugLevel, tr("Произошло перетерание не до конца прочитанного кадра"));
        }
        if (status.Flags & LTR210_STATUS_FLAG_SYNC_SKIP)
        {
            m_tbl->item(row, column_skip_sync)->setText(locale.toString(m_tbl->item(row, column_skip_sync)->text().toInt()+1));
            m_tbl->item(row, column_skip_sync)->setBackgroundColor(Qt::darkYellow);
            //dev->reportMessage(QxtLogger::DebugLevel, tr("Было пропущено один или несколько сигналов SYNC"));
        }
        if (status.Flags & LTR210_STATUS_FLAG_INVALID_HIST)
        {
            m_tbl->item(row, column_inv_hist)->setText(locale.toString(m_tbl->item(row, column_inv_hist)->text().toInt()+1));
            m_tbl->item(row, column_inv_hist)->setBackgroundColor(Qt::darkYellow);
            //dev->reportMessage(QxtLogger::DebugLevel, tr("Был принят кадр с неверной предысторией"));
        }

    }
}

void SignalParamsPanel::on_itemDoubleClicked(QTableWidgetItem *item)
{
    if (item->column()==column_color)
    {
        QColor color = QColorDialog::getColor(item->backgroundColor(), this, tr("Выбор цвета для отображения канала на графике"));
        if (color.isValid())
        {
            item->setBackgroundColor(color);
            emit colorChanged(item->row(), color);
        }
    }
}

void SignalParamsPanel::on_itemChanged(QTableWidgetItem *item)
{
    if (item->column()==column_ch)
    {
        bool val = item->checkState()==Qt::Checked;
        if (item->row() < m_ch_en.size())
            m_ch_en[item->row()] = val;
        emit plotEnChanged(item->row(), val);
    }
}
