#include "OscMainWindow.h"
#include "ui_OscMainWindow.h"
#include <QVBoxLayout>
#include <QTimer>
#include <QSettings>
#include <QMessageBox>
#include <math.h>
#include <QFileDialog>
#include <QTableWidget>
#include <QHeaderView>
#include <QTreeWidgetItem>
#include <QHBoxLayout>
#include <QComboBox>
#include <QDoubleSpinBox>
#include <QFormLayout>
#include <QxtLogger>
#include "LQxtTableLoggerEngine.h"


#include "QLedIndicator.h"
#include "lmath.h"
#include "SignalParamsPanel.h"
#include "LQTableWidget.h"
#include "DevSelectDialog.h"



static void led_set_red(QLedIndicator* led)
{
    led->setOnColor1(QColor(255,0,0));
    led->setOnColor2(QColor(192,0,0));
    led->setOffColor1(QColor(28,0,0));
    led->setOffColor2(QColor(128,0,0));
}

static void led_set_green(QLedIndicator* led)
{
    led->setOnColor1(QColor(0,255,0));
    led->setOnColor2(QColor(0,192,0));
    led->setOffColor1(QColor(0,28,0));
    led->setOffColor2(QColor(0,128,0));
}


OscMainWindow::OscMainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::OscMainWindow),
    ledBlink(false), run(false), m_graphUpdate(false)
{
    ui->setupUi(this);
    this->setWindowTitle(QCoreApplication::applicationName() + " - " + QCoreApplication::applicationVersion());

    qRegisterMetaType<TLTR210_FRAME_STATUS>("TLTR210_FRAME_STATUS");
    qRegisterMetaType<LQChFrameParams>("LQChFrameParams");



    /* добавляем индикатор прихода кадров на панель инструментов */
    m_progLed = new QLedIndicator();
    m_progLed->setEnabled(false);
    ui->toolBar->addSeparator();
    ui->toolBar->addWidget(m_progLed);


    /* основное окно состоит только из двух графиков, расположенных вертикально */
    QVBoxLayout* lout = new QVBoxLayout();
    m_timeGraph = new LQGraphWgt("TimeGraph");
    m_timeGraph->setYScale(-10., +10.);
    m_timeGraph->setXScale(0, 1000);
    lout->addWidget(m_timeGraph);
    m_graphs.append(m_timeGraph);

    m_dinGraph = new LQGraphWgt("DinGraph");
    m_dinGraph->setYScale(0,1);
    m_dinGraph->setXScale(0, 1000);
    m_dinGraph->setYGridStep(1);
    lout->addWidget(m_dinGraph,10);
    m_graphs.append(m_dinGraph);

    connect(m_timeGraph, SIGNAL(xAutoScaleChanged(bool)), m_dinGraph, SLOT(setXAutoScale(bool)));
    connect(m_timeGraph, SIGNAL(xAutoScaleChanged(bool)), m_dinGraph, SLOT(graphUpdate()));
    connect(m_timeGraph, SIGNAL(xScaleChanged(double,double)), m_dinGraph, SLOT(setXScale(double,double)));
    connect(m_timeGraph, SIGNAL(xScaleChanged(double,double)), m_dinGraph, SLOT(graphUpdate()));
    connect(m_dinGraph, SIGNAL(xAutoScaleChanged(bool)), m_timeGraph, SLOT(setXAutoScale(bool)));
    connect(m_dinGraph, SIGNAL(xAutoScaleChanged(bool)), m_timeGraph, SLOT(graphUpdate()));
    connect(m_dinGraph, SIGNAL(xScaleChanged(double,double)), m_timeGraph, SLOT(setXScale(double,double)));
    connect(m_dinGraph, SIGNAL(xScaleChanged(double,double)), m_timeGraph, SLOT(graphUpdate()));

    m_fftGraph = new LQGraphWgt("FftGraph");
    m_fftGraph->setYScale(-140., 0);
    m_fftGraph->setXScale(0, 1000);
    m_fftGraph->setXLogModeEnabled(true);
    //m_fftGraph->setXLogMode(true);
    lout->addWidget(m_fftGraph);
    m_graphs.append(m_fftGraph);



#ifdef LTR210_OSC_ENABLE_HIST
    m_histGraph = new LQGraphWgt("HistGraph", this, LQGraphWgt::MODE_HIST);
    m_histGraph->setYScale(0,1);
    m_histGraph->setXScale(-10, 10);
    m_histGraph->setXAutoScale(true);
    m_histGraph->setYAutoScale(true);
    m_histGraph->setShowPointsCnt(0);
    lout->addWidget(m_histGraph);
    m_graphs.append(m_histGraph);
#endif

    this->centralWidget()->setLayout(lout);




    /* создаем диалог, который будем использовать для индикации прогресса
     * загрузки ПЛИС модулей */
    m_progrDialog =  new QProgressDialog(tr("Загрузка прошивки ПЛИС"),0, 0, 100, this, Qt::Dialog | Qt::MSWindowsFixedSizeDialogHint
                                         | Qt::CustomizeWindowHint | Qt::WindowTitleHint);
    m_progrDialog->setMinimumWidth(350);
    m_progrDialog->setWindowModality(Qt::WindowModal);


    /* создаем дополнительную панель с параметрами принятого кадра */
    m_paramPanel = new SignalParamsPanel();
    this->addDockWidget(Qt::BottomDockWidgetArea, m_paramPanel);
    connect(m_paramPanel, SIGNAL(colorChanged(int,QColor)), SLOT(onColorChanged(int,QColor)));
    connect(m_paramPanel, SIGNAL(plotEnChanged(int,bool)), SLOT(onPlotEnChanged(int,bool)));


    ui->logPanel->setVisible(false);
    /* загрузка настроек состояния интерфейса */
#ifdef Q_WS_WIN
    /* на Windows используем ini-файлы, а не реестр (как по умолчанию) */
    QSettings::setDefaultFormat(QSettings::IniFormat);
#endif

    QSettings set;
    set.beginGroup("MainWindow");
    restoreState(set.value("state").toByteArray());
    resize(set.value("size", QSize(800, 600)).toSize());
    move(set.value("pos", QPoint(0, 0)).toPoint());

    if ((set.value("sigPramColumnsState").toByteArray().size()!=0)
            && (set.value("sigPramColumnsCnt").toInt()==m_paramPanel->table()->columnCount()))
    {
        m_paramPanel->table()->horizontalHeader()->restoreState(set.value("sigPramColumnsState").toByteArray());
    }

    foreach (LQGraphWgt* graph, m_graphs)
    {
        graph->loadSettings(set, graph->name());
    }

    ui->actionShowTimeSig->setChecked(set.value("show_time_sig", true).toBool());
    ui->actionShowFFT->setChecked(set.value("show_fft", true).toBool());
    ui->actionShowHist->setChecked(set.value("show_hist", false).toBool());
    ui->actionShowDin->setChecked(set.value("show_din", false).toBool());



    set.endGroup();

    set.beginGroup("AdcCfg");
    ui->FrameSize->setValue(set.value("frame_size", 1000).toInt());
    ui->HistSize->setValue(set.value("hist_size", 0).toInt());
    ui->AdcFreqDiv->setValue(set.value("adc_freq_div", 1).toInt());
    ui->AdcDcm->setValue(set.value("adc_dcm_cnt", 1).toInt());
    ui->FrameFreqDiv->setValue(set.value("frame_freq_div",1000000).toInt());

    ui->testMode->setChecked(set.value("test_mode", false).toBool());
    ui->writeAutoSusp->setChecked(set.value("write_autosusp", true).toBool());
    ui->keepaliveStatusSend->setChecked(set.value("keepalive_en", true).toBool());
    ui->showCodes->setChecked(set.value("adc_codes", false).toBool());
    ui->afcCorrect->setChecked(set.value("afc_correction", true).toBool());
    ui->zeroOffsCor->setChecked(set.value("zero_offs_correction", false).toBool());

    ui->intfTransfRate->setCurrentIndex(set.value("intf_transf_rate", 0).toInt());

    ui->actionAutoupAdcCfg->setChecked(set.value("auto_update_cfg", true).toBool());

    set.endGroup();




    QList<QColor> colors;
    QList<bool> en;
    for (int ch=0; ch < m_timeGraph->maxPlotCount();ch++)
    {
        colors.append(m_timeGraph->plotColor(ch));
        en.append(m_timeGraph->plotIsEnabled(ch));
    }
    m_paramPanel->setColors(colors);
    m_paramPanel->setChEnabled(en);

    connect(ui->actionShowDevCfgPanel, SIGNAL(triggered(bool)), ui->configPanel,
            SLOT(setVisible(bool)));
    connect(ui->configPanel, SIGNAL(visibilityChanged(bool)), ui->actionShowDevCfgPanel,
            SLOT(setChecked(bool)));
    connect(ui->actionShowSigParamPanel, SIGNAL(triggered(bool)), m_paramPanel,
            SLOT(setVisible(bool)));
    connect(m_paramPanel, SIGNAL(visibilityChanged(bool)), ui->actionShowSigParamPanel,
            SLOT(setChecked(bool)));
    connect(ui->actionShowLogPanel, SIGNAL(triggered(bool)), ui->logPanel,
            SLOT(setVisible(bool)));
    connect(ui->logPanel, SIGNAL(visibilityChanged(bool)), ui->actionShowLogPanel,
            SLOT(setChecked(bool)));

#ifdef LTR210_OSC_ENABLE_HIST
    connect(ui->actionShowHist, SIGNAL(triggered(bool)), m_histGraph, SLOT(setVisible(bool)));
    m_histGraph->setVisible(ui->actionShowHist->isChecked());
#endif
    connect(ui->actionShowTimeSig, SIGNAL(triggered(bool)), m_timeGraph, SLOT(setVisible(bool)));
    m_timeGraph->setVisible(ui->actionShowTimeSig->isChecked());
    connect(ui->actionShowFFT, SIGNAL(triggered(bool)), m_fftGraph, SLOT(setVisible(bool)));
    m_fftGraph->setVisible(ui->actionShowFFT->isChecked());
    connect(ui->actionShowDin, SIGNAL(triggered(bool)), m_dinGraph, SLOT(setVisible(bool)));
    m_dinGraph->setVisible(ui->actionShowDin->isChecked());


    m_tblLog = new LQxtTableLoggerEngine((QxtLogger::LogLevel)set.value("log_lvl", QxtLogger::InfoLevel).toInt(), this);
    qxtLog->addLoggerEngine(QString("tableLog"), m_tblLog);
    ui->logPanel->widget()->layout()->addWidget(m_tblLog->widget());



    m_sigProc.moveToThread(&m_sigProcThread);
    m_sigProcThread.start();
    connect(&m_sigProc, SIGNAL(frameProcessed(QLtr210*,LQChFrameParams,LQChFrameParams,TLTR210_FRAME_STATUS)),
            SLOT(onFrameRecv(QLtr210*,LQChFrameParams,LQChFrameParams,TLTR210_FRAME_STATUS)));


    paramsChanged();

    qxtLog->info(tr("Приложение запущено"));
}

OscMainWindow::~OscMainWindow()
{
    delete ui;
}

void OscMainWindow::closeEvent(QCloseEvent *event)
{
    if (run)
        on_actionStop_triggered();



    saveSettings();
    devsClose();


    m_sigProcThread.exit();
    m_sigProcThread.wait();
}

void OscMainWindow::saveSettings()
{
    QSettings set;
    set.beginGroup("MainWindow");
    /* если коно в настоящий момент не свернуто - то сохраняем его текущие
       параметры */
    if (this->isVisible() && !isMinimized())
    {
        set.setValue("state", saveState());
        set.setValue("size", size());
        set.setValue("pos", pos());
    }

    set.setValue("sigPramColumnsState", m_paramPanel->table()->horizontalHeader()->saveState());
    set.setValue("sigPramColumnsCnt", m_paramPanel->table()->columnCount());
    //set.setValue("configPanel", ui->configPanel->saveState());

    foreach (LQGraphWgt* graph, m_graphs)
    {
        graph->saveSettings(set, graph->name());
    }


    set.setValue("show_time_sig", ui->actionShowTimeSig->isChecked());
    set.setValue("show_fft", ui->actionShowFFT->isChecked());
    set.setValue("show_hist",ui->actionShowHist->isChecked());
    set.setValue("show_din", ui->actionShowDin->isChecked());


    set.endGroup();

    set.beginGroup("AdcCfg");
    set.setValue("frame_size", ui->FrameSize->value());
    set.setValue("hist_size", ui->HistSize->value());
    set.setValue("adc_freq_div", ui->AdcFreqDiv->value());
    set.setValue("adc_dcm_cnt", ui->AdcDcm->value());
    set.setValue("frame_freq_div", ui->FrameFreqDiv->value());
    set.setValue("write_autosusp", ui->writeAutoSusp->isChecked());
    set.setValue("test_mode", ui->testMode->isChecked());
    set.setValue("keepalive_en", ui->keepaliveStatusSend->isChecked());
    set.setValue("adc_codes", ui->showCodes->isChecked());
    set.setValue("afc_correction", ui->afcCorrect->isChecked());
    set.setValue("zero_offs_correction", ui->zeroOffsCor->isChecked());
    set.setValue("intf_transf_rate", ui->intfTransfRate->currentIndex());

    set.setValue("auto_update_cfg", ui->actionAutoupAdcCfg->isChecked());


    set.endGroup();



    for (int i=0; i < m_devInfoList.size(); i++)
    {
        QLtr210Info* pinfo = m_devInfoList[i];
        /* сохраняем настройки каждого устройства */
        set.beginGroup("DevCfg"+QString::number(i));

        for (unsigned ch=0; ch < LTR210_CHANNEL_CNT; ch++)
        {
            set.beginGroup("Ch" + QString::number(ch+1));
            set.setValue("en", pinfo->ui.ch[ch].group->isChecked());
            set.setValue("range", pinfo->ui.ch[ch].range->currentIndex());
            set.setValue("mode", pinfo->ui.ch[ch].mode->currentIndex());
            set.setValue("dataBitMode", pinfo->ui.ch[ch].dataBitMode->currentIndex());
            set.setValue("sync_level_l", pinfo->ui.ch[ch].syncLvlL->value());
            set.setValue("sync_level_h", pinfo->ui.ch[ch].syncLvlH->value());
            set.endGroup();
        }


        set.setValue("sync mode", pinfo->ui.syncMode->currentIndex());
        set.setValue("group_mode", pinfo->ui.groupMode->currentIndex());

        set.endGroup();
    }

    set.beginWriteArray("last_devs");
    for (int i=0; i < m_devInfoList.size(); i++)
    {
        set.setArrayIndex(i);
        set.setValue("serial", m_devInfoList[i]->device->serial());
    }
    set.endArray();


    set.setValue("log_lvl", m_tblLog->logLevel());
}

void OscMainWindow::clearPlot(int plot_id)
{
    foreach (LQGraphWgt* graph, m_graphs)
    {
        graph->clearPlot(plot_id);
    }
}


/* запись настроек из интерфейса во все модули */
int OscMainWindow::setDevConfig()
{
    TLTR210_CONFIG Cfg;    
    memset(&Cfg, 0, sizeof(Cfg));

    int err = LTR_OK;


    Cfg.FrameSize = ui->FrameSize->value();
    Cfg.HistSize = ui->HistSize->value();
    Cfg.AdcDcmCnt = ui->AdcDcm->value() - 1;
    Cfg.AdcFreqDiv = ui->AdcFreqDiv->value() - 1;
    Cfg.FrameFreqDiv = ui->FrameFreqDiv->value()-1;
    Cfg.IntfTransfRate = ui->intfTransfRate->currentIndex();


    Cfg.Flags = 0;
    if (ui->testMode->isChecked())
    {
        Cfg.Flags |= LTR210_CFG_FLAGS_TEST_CNTR_MODE;
        /* тестовый режим работает только на максимальной частоте */
        Cfg.AdcDcmCnt = 0;
        Cfg.AdcFreqDiv = 0;
    }

    if (ui->keepaliveStatusSend->isChecked())
        Cfg.Flags |= LTR210_CFG_FLAGS_KEEPALIVE_EN;
    if (ui->writeAutoSusp->isChecked())
        Cfg.Flags |= LTR210_CFG_FLAGS_WRITE_AUTO_SUSP;


    foreach (QLtr210Info* pinfo, m_devInfoList)
    {
        for (unsigned ch=0; ch < LTR210_CHANNEL_CNT; ch++)
        {
            /* в тестовом режиме всегда используем только первый канал */
            if (Cfg.Flags & LTR210_CFG_FLAGS_TEST_CNTR_MODE)
            {
                Cfg.Ch[ch].Enabled = (ch==0);
            }
            else
            {
                Cfg.Ch[ch].Enabled = pinfo->ui.ch[ch].group->isChecked();
            }

            Cfg.Ch[ch].Range = pinfo->ui.ch[ch].range->currentIndex();
            Cfg.Ch[ch].Mode = pinfo->ui.ch[ch].mode->currentIndex();
            Cfg.Ch[ch].DigBitMode = pinfo->ui.ch[ch].dataBitMode->currentIndex();
            Cfg.Ch[ch].SyncLevelL = pinfo->ui.ch[ch].syncLvlL->value();
            Cfg.Ch[ch].SyncLevelH = pinfo->ui.ch[ch].syncLvlH->value();
        }


        Cfg.SyncMode = pinfo->ui.syncMode->currentIndex();
        Cfg.GroupMode = pinfo->ui.groupMode->currentIndex();

        int set_err = pinfo->device->setConfig(&Cfg);
        if (err==LTR_OK)
            err = set_err;

        pinfo->device->setCodes(ui->showCodes->isChecked());
        pinfo->device->setAfcCorrection(ui->afcCorrect->isChecked());
        pinfo->device->setZeroCorrection(ui->zeroOffsCor->isChecked());
    }
    return err;
}

/* разрешение или запрещение элементов для настройки параметров, которые
 * могут изменяться только при остановленном сборе данных */
void OscMainWindow::configUiEnable(bool en)
{
    ui->FrameSize->setEnabled(en);
    ui->HistSize->setEnabled(en);
    ui->AdcDcm->setEnabled(en);
    ui->AdcFreqDiv->setEnabled(en);
    ui->writeAutoSusp->setEnabled(en);
    ui->testMode->setEnabled(en);    
    ui->keepaliveStatusSend->setEnabled(en);
    ui->intfTransfRate->setEnabled(en);
    ui->adcFreq->setEnabled(en);
    ui->setAdcFreq->setEnabled(en);
    ui->zeroOffsCor->setEnabled(en);



    foreach (QLtr210Info* pinfo, m_devInfoList)
    {
        pinfo->ui.syncMode->setEnabled(en);
        pinfo->ui.groupMode->setEnabled(en);

        for (int ch=0; ch < LTR210_CHANNEL_CNT; ch++)
        {
            pinfo->ui.ch[ch].dataBitMode->setEnabled(en);
            /* при включенной коррекции нуля нельзя изменять диапазоны на лету */
            pinfo->ui.ch[ch].range->setEnabled(en || !ui->zeroOffsCor->isChecked());
        }
    }
}

void OscMainWindow::addDevice(QLtr210 *dev)
{
    QLtr210Info *pinfo = new QLtr210Info;
    pinfo->device = dev;

    /* Создаем элементы интерфейса для настройки нового устройства, которые
       добавляем в дерево устройств */
    pinfo->ui.devItem = new QTreeWidgetItem();
    QWidget* devWgt = new QWidget();
    QHBoxLayout* lout = new QHBoxLayout();
    QLabel* devstr = new QLabel(dev->devDescr());
    pinfo->ui.runLed = new QLedIndicator(15);
    pinfo->ui.progLed = new QLedIndicator(15);
    pinfo->ui.errLed = new QLedIndicator(15);

    connect(dev, SIGNAL(devDescrChanged(QString)), devstr, SLOT(setText(QString)));

    //pinfo->ui.progLed->setBlinkInterval(100);
    //pinfo->ui.errLed->setBlinkInterval(100);

    led_set_red(pinfo->ui.errLed);
    pinfo->ui.runLed->setEnabled(false);
    pinfo->ui.progLed->setEnabled(false);
    pinfo->ui.errLed->setEnabled(false);

    lout->addWidget(devstr);
    lout->addStretch(10);
    lout->addWidget(pinfo->ui.runLed);
    lout->addWidget(pinfo->ui.progLed);
    lout->addWidget(pinfo->ui.errLed);
    devWgt->setLayout(lout);



    pinfo->ui.cfgItem = new QTreeWidgetItem();
    QWidget* cfgWgt = new QWidget();
    QGridLayout* cfg_lout = new QGridLayout();
    for (unsigned ch=0; ch < LTR210_CHANNEL_CNT; ch++)
    {

        pinfo->ui.ch[ch].group = new QGroupBox();
        pinfo->ui.ch[ch].group->setTitle(tr("Канал ") + QString::number(ch+1));

        pinfo->ui.ch[ch].group->setCheckable(true);

        pinfo->ui.ch[ch].range = new QComboBox();
        for (unsigned range = 0; range < LTR210_RANGE_CNT; range++)
        {
            pinfo->ui.ch[ch].range->addItem(QString("+/- %0 В").arg(QString::number(f_range_vals[range])));
        }
        pinfo->ui.ch[ch].mode = new QComboBox();
        pinfo->ui.ch[ch].mode->addItem(tr("Открытый вход"));
        pinfo->ui.ch[ch].mode->addItem(tr("Закрытый вход"));
        pinfo->ui.ch[ch].mode->addItem(tr("Собственный ноль"));



        pinfo->ui.ch[ch].dataBitMode = new QComboBox();
        pinfo->ui.ch[ch].dataBitMode->addItem(tr("Нуль"));
        pinfo->ui.ch[ch].dataBitMode->addItem(tr("Вход SYNC"));
        pinfo->ui.ch[ch].dataBitMode->addItem(tr("Канал АЦП 1"));
        pinfo->ui.ch[ch].dataBitMode->addItem(tr("Канал АЦП 2"));
        pinfo->ui.ch[ch].dataBitMode->addItem(tr("Внутр. событие"));


        pinfo->ui.ch[ch].syncLvlL = new QDoubleSpinBox();
        pinfo->ui.ch[ch].syncLvlL->setMinimum(-10.);
        pinfo->ui.ch[ch].syncLvlL->setMaximum(10.);

        pinfo->ui.ch[ch].syncLvlH = new QDoubleSpinBox();
        pinfo->ui.ch[ch].syncLvlH->setMinimum(-10.);
        pinfo->ui.ch[ch].syncLvlH->setMaximum(10.);




        if (ch==0)
        {
            QFormLayout* cur_ch_lout = new QFormLayout();
            cur_ch_lout->addRow(tr("Диапазон"),  pinfo->ui.ch[ch].range);
            cur_ch_lout->addRow(tr("Режим"),     pinfo->ui.ch[ch].mode);
            cur_ch_lout->addRow(tr("Ниж. ур. синхр."), pinfo->ui.ch[ch].syncLvlL);
            cur_ch_lout->addRow(tr("Верх. ур. синхр."), pinfo->ui.ch[ch].syncLvlH);
            cur_ch_lout->addRow(tr("Спец. бит"), pinfo->ui.ch[ch].dataBitMode);
            pinfo->ui.ch[ch].group->setLayout(cur_ch_lout);
            cfg_lout->addWidget(pinfo->ui.ch[ch].group, 0, ch);
        }
        else
        {
            QVBoxLayout* cur_ch_lout = new QVBoxLayout();
            cur_ch_lout->addWidget(pinfo->ui.ch[ch].range);
            cur_ch_lout->addWidget(pinfo->ui.ch[ch].mode);
            cur_ch_lout->addWidget(pinfo->ui.ch[ch].syncLvlL);
            cur_ch_lout->addWidget(pinfo->ui.ch[ch].syncLvlH);
            cur_ch_lout->addWidget(pinfo->ui.ch[ch].dataBitMode);
            pinfo->ui.ch[ch].group->setLayout(cur_ch_lout);
            cfg_lout->addWidget(pinfo->ui.ch[ch].group, 0, ch);
        }

        connect(pinfo->ui.ch[ch].group, SIGNAL(toggled(bool)), SLOT(onAdcSetChanged()));
        connect(pinfo->ui.ch[ch].range, SIGNAL(currentIndexChanged(int)), SLOT(onAdcSetChanged()));
        connect(pinfo->ui.ch[ch].mode, SIGNAL(currentIndexChanged(int)), SLOT(onAdcSetChanged()));
        connect(pinfo->ui.ch[ch].syncLvlL, SIGNAL(valueChanged(double)), SLOT(onAdcSetChanged()));
        connect(pinfo->ui.ch[ch].syncLvlH, SIGNAL(valueChanged(double)), SLOT(onAdcSetChanged()));
    }

    cfg_lout->setColumnStretch(0, 20);
    QGroupBox* syncBox = new QGroupBox();
    syncBox->setTitle(tr("Настройки синхронизации"));
    QFormLayout* syncLout = new QFormLayout();

    pinfo->ui.syncMode = new QComboBox();
    pinfo->ui.syncMode->addItem(tr("Внутренняя (программная)"));
    pinfo->ui.syncMode->addItem(tr("Канал 1 (фронт)"));
    pinfo->ui.syncMode->addItem(tr("Канал 1 (спад)"));
    pinfo->ui.syncMode->addItem(tr("Канал 2 (фронт)"));
    pinfo->ui.syncMode->addItem(tr("Канал 2 (спад)"));
    pinfo->ui.syncMode->addItem(tr("SYNC IN (фронт)"));
    pinfo->ui.syncMode->addItem(tr("SYNC IN (спад)"));
    pinfo->ui.syncMode->addItem(tr("Периодический"));
    pinfo->ui.syncMode->addItem(tr("Непрерывный сбор"));
    syncLout->addRow(tr("Режим синхронизации"), pinfo->ui.syncMode);

    pinfo->ui.groupMode = new QComboBox();
    pinfo->ui.groupMode->addItem(tr("Откл."));
    pinfo->ui.groupMode->addItem(tr("Мастер"));
    pinfo->ui.groupMode->addItem(tr("Подчиненный (slave)"));
    syncLout->addRow(tr("Работа в группе"), pinfo->ui.groupMode);


    syncBox->setLayout(syncLout);


    cfg_lout->addWidget(syncBox, 1, 0, 1, 2);
    cfgWgt->setLayout(cfg_lout);

    ui->devTree->addTopLevelItem(pinfo->ui.devItem);
    ui->devTree->setItemWidget(pinfo->ui.devItem, 0, devWgt);

    pinfo->ui.devItem->addChild(pinfo->ui.cfgItem);
    ui->devTree->setItemWidget(pinfo->ui.cfgItem, 0, cfgWgt);

    ui->devTree->expandAll();


    /* соединяем сигналы для отображения работы класса устройства в UI */
    connect(dev, SIGNAL(error(QString, int)), SLOT(onError(QString, int)));
    connect(dev, SIGNAL(frameRecvd(LQChFrameParams,LQChFrameParams,TLTR210_FRAME_STATUS,int)),
            &m_sigProc, SLOT(processFrame(LQChFrameParams,LQChFrameParams,TLTR210_FRAME_STATUS,int)));
    connect(dev, SIGNAL(loadFpgaStart(quint32)), SLOT(onLoadFpgaStart(quint32)));
    connect(dev, SIGNAL(loadFpgaProgr(quint32,quint32)), SLOT(onLoadFpgaProgr(quint32,quint32)));
    connect(dev, SIGNAL(loadFpgaStop(qint32)), SLOT(onLoadFpgaDone(qint32)));



    /* устанавливаем начальные настройки в соответствие с сохраненными */
    QSettings set;
    int last_dev_cnt = set.beginReadArray("last_devs");
    set.endArray();
    int set_devnum = 0;

    /* если устройств было в последний раз меньше, то последующим
     * устройствам устанавливаем настройки, соответствующие последнему */
    if (m_devInfoList.size()>last_dev_cnt)
    {
        set_devnum = last_dev_cnt < 0 ? 0 : last_dev_cnt;
    }
    else
    {
        set_devnum = m_devInfoList.size();
    }

    set.beginGroup("DevCfg"+QString::number(set_devnum));

    for (unsigned ch=0; ch < LTR210_CHANNEL_CNT; ch++)
    {
        set.beginGroup("Ch" + QString::number(ch+1));
        pinfo->ui.ch[ch].group->setChecked(set.value("en", true).toBool());
        pinfo->ui.ch[ch].range->setCurrentIndex(set.value("range").toInt());        
        pinfo->ui.ch[ch].mode->setCurrentIndex(set.value("mode").toInt());
        pinfo->ui.ch[ch].dataBitMode->setCurrentIndex(set.value("dataBitMode", LTR210_DIG_BIT_MODE_SYNC_IN).toInt());
        pinfo->ui.ch[ch].syncLvlL->setValue(set.value("sync_level_l", 0).toDouble());
        pinfo->ui.ch[ch].syncLvlH->setValue(set.value("sync_level_h", 0).toDouble());
        set.endGroup();
    }

    pinfo->ui.syncMode->setCurrentIndex(set.value("sync mode", 0).toInt());
    pinfo->ui.groupMode->setCurrentIndex(set.value("group_mode", 0).toInt());

    set.endGroup();


    m_devInfoList << pinfo;
    m_paramPanel->addDevice(dev);
}

void OscMainWindow::on_actionStart_triggered()
{
    int err = setDevConfig();
    if (err==LTR_OK)
    {
        run = true;

        ui->actionStart->setEnabled(false);
        ui->actionLoadFPGA->setEnabled(false);
        ui->actionRefreshDevList->setEnabled(false);

        configUiEnable(false);

        bool en_prog_frame = false;

        m_paramPanel->resetParams();

        QList<QLtr210Info*> p_masters;

        for (int i=0; i < m_devInfoList.size(); i++)
        {
            QLtr210Info* pinfo = m_devInfoList[i];

            for (unsigned ch=0; ch < LTR210_CHANNEL_CNT; ch++)
            {
                clearPlot(LTR210_CHANNEL_CNT*i+ch);
            }


            if ((pinfo->device->config().SyncMode == LTR210_SYNC_MODE_INTERNAL) &&
                    (pinfo->device->config().GroupMode!=LTR210_GROUP_MODE_SLAVE))
            {
                en_prog_frame = true;
            }

            if (pinfo->ui.runLed->isChecked())
            {
                pinfo->ui.runLed->setChecked(false);
            }

            led_set_green(pinfo->ui.runLed);
            pinfo->ui.runLed->setChecked(true);
        }

        /* выполняем измерение смещения нуля, если требуется  */
        if (ui->zeroOffsCor->isChecked())
        {
            for (int i=0; (i < m_devInfoList.size()); i++)
            {
                err = m_devInfoList[i]->device->measZero();
            }
        }

        /* запускаем вначале модули которые работают индивидуально или
         * подчиненные, а настроенные на режим мастера сохраняем
         * в список, чтобы запустить их последними */
        for (int i=0; (i < m_devInfoList.size()); i++)
        {
            QLtr210Info* pinfo = m_devInfoList[i];
            if (pinfo->device->config().GroupMode==LTR210_GROUP_MODE_MASTER)
            {
                p_masters.append(pinfo);
            }
            else
            {
                err = pinfo->device->startADC();
            }
        }



        foreach (QLtr210Info* pinfo, p_masters)
        {
            err = pinfo->device->startADC();
        }


        foreach (QLtr210Info* pinfo, m_devInfoList)
        {
            pinfo->device->moveToThread(pinfo->device);
            pinfo->device->start();
        }

        ui->actionStop->setEnabled(true);
        ui->actionFrameStart->setEnabled(en_prog_frame);

        graphUpdate();
    }   
}

void OscMainWindow::showPlot(QLtr210* dev, int plot_id, int ch, LQChFrameParams* ch_par)
{
    if (ch_par->size)
    {
        if (ch_par->data)
            m_timeGraph->plotData(plot_id, ch_par->data, ch_par->size, 1000.*ch_par->dt);
        if (ch_par->din)
            m_dinGraph->plotData(plot_id, ch_par->din, ch_par->size, 1000.*ch_par->dt);
        if (ch_par->fft)
            m_fftGraph->plotData(plot_id, ch_par->fft, ch_par->fft_size, ch_par->df);

#ifdef LTR210_OSC_ENABLE_HIST
        m_histGraph->plotData(plot_id, ch_par->data, ch_par->size, ch_par->range_val/LTR210_ADC_SCALE_CODE_MAX);
#endif

        m_paramPanel->showParams(dev, ch, ch_par->avg, ch_par->rms, ch_par->f_max, ch_par->snr,
                                 ch_par->thd, ch_par->sinad, ch_par->sfdr, ch_par->enob);
    }
    else
    {
        clearPlot(plot_id);
    }
}

void OscMainWindow::devsClose()
{
    for (int i=0; i < m_devInfoList.size(); i++)
    {
        QLtr210Info *pinfo = m_devInfoList[i];
        pinfo->device->close();
        disconnect(pinfo->device, SIGNAL(error(QString, int)), this, SLOT(onError(QString, int)));
        disconnect(pinfo->device, SIGNAL(loadFpgaStart(quint32)), this, SLOT(onLoadFpgaStart(quint32)));
        disconnect(pinfo->device, SIGNAL(loadFpgaProgr(quint32,quint32)), this, SLOT(onLoadFpgaProgr(quint32,quint32)));
        disconnect(pinfo->device, SIGNAL(loadFpgaStop(qint32)), this, SLOT(onLoadFpgaDone(qint32)));
        disconnect(pinfo->device, SIGNAL(frameRecvd(LQChFrameParams,LQChFrameParams,TLTR210_FRAME_STATUS,int)),
                &m_sigProc, SLOT(processFrame(LQChFrameParams,LQChFrameParams,TLTR210_FRAME_STATUS,int)));


        for (unsigned ch=0; ch < LTR210_CHANNEL_CNT; ch++)
        {
            clearPlot(i*LTR210_CHANNEL_CNT+ch);
        }

        delete pinfo->device;
        delete pinfo;
    }

    graphUpdate();


    ui->devTree->clear();
    m_paramPanel->clear();
    m_devInfoList.clear();
}


void OscMainWindow::onFrameRecv(QLtr210 *dev, LQChFrameParams ch1, LQChFrameParams ch2, TLTR210_FRAME_STATUS status)
{
    int plot_id=-1;
    QLtr210Info* pinfo;

    /* находим устройтсво в списке, чтобы узнать его номер для отображения */
    for (int i=0; (plot_id < 0) && (i < m_devInfoList.size()); i++)
    {
        if (m_devInfoList[i]->device==dev)
        {
            plot_id = LTR210_CHANNEL_CNT*i;
            pinfo = m_devInfoList[i];
        }
    }

    if (plot_id>=0)
    {
        showPlot(dev, plot_id, 0, &ch1);
        showPlot(dev, plot_id+1, 1, &ch2);





        if (!m_graphUpdate)
        {
            m_graphUpdate = true;
            QTimer::singleShot(200, this, SLOT(graphUpdate()));
            //graphUpdate();
        }


        if (status.Result == LTR210_FRAME_RESULT_OK)
        {
            m_progLed->blink();
            pinfo->ui.progLed->blink();
        }
        else
        {
            pinfo->ui.errLed->blink();
        }

        m_paramPanel->updateFrameCntr(dev, status);
    }

    delete ch1.data;
    delete ch1.fft;
    delete ch1.din;

    delete ch2.data;
    delete ch2.fft;
    delete ch2.din;
}

void OscMainWindow::onError(QString errTitle, int err)
{
    QLtr210* dev = qobject_cast<QLtr210*>(sender());
    if (dev)
    {
        /* проверяем - осталось ли хоть одно запущенное устройство. Если
           нет, то считаем что сбор остановлен */
        bool remain_run = false;
        QLtr210Info *pinfo=0;
        for (int i=0; (i < m_devInfoList.size()) && (pinfo==0); i++)
        {
            if (m_devInfoList[i]->device==dev)
            {
                pinfo = m_devInfoList[i];
            }
        }

        led_set_red(pinfo->ui.runLed);
        pinfo->ui.runLed->setChecked(true);

        QMessageBox::critical(this, dev->devDescr(), errTitle + ": " + dev->errorString(err));

        for (int i=0; (i < m_devInfoList.size()) && (remain_run==false); i++)
        {
            if (m_devInfoList[i]->device->isRunning())
            {
                remain_run = true;
            }
        }

        if (run && !remain_run)
            on_actionStop_triggered();
    }
}

void OscMainWindow::onLoadFpgaStart(quint32 size)
{
    m_progrDialog->setMaximum(size);
    m_progrDialog->setValue(0);
    m_progrDialog->show();
}

void OscMainWindow::onLoadFpgaProgr(quint32 done_size, quint32 total_size)
{
    m_progrDialog->setValue(done_size);
    QCoreApplication::flush();
    QCoreApplication::processEvents();
}

void OscMainWindow::onLoadFpgaDone(qint32 err)
{
    //m_progrDialog->hide();
}

void OscMainWindow::on_actionStop_triggered()
{
    ui->actionStop->setEnabled(false);
    ui->actionFrameStart->setEnabled(false);

    foreach (QLtr210Info *pinfo, m_devInfoList)
    {
        pinfo->device->stopRequest();
    }

    foreach (QLtr210Info *pinfo, m_devInfoList)
    {
        pinfo->device->wait();
        pinfo->ui.runLed->setChecked(false);
    }

    ui->actionStart->setEnabled(true);
    ui->actionLoadFPGA->setEnabled(true);
    ui->actionRefreshDevList->setEnabled(true);
    run = false;

    configUiEnable(true);
}

void OscMainWindow::on_actionFrameStart_triggered()
{
    foreach (QLtr210Info *pinfo, m_devInfoList)
    {
        if ((pinfo->device->config().SyncMode==LTR210_SYNC_MODE_INTERNAL)
                && (pinfo->device->config().GroupMode!=LTR210_GROUP_MODE_SLAVE))
        {
            pinfo->device->startFrame();
        }
    }
}

void OscMainWindow::on_actionLoadFPGA_triggered()
{
    QString fileName = QFileDialog::getOpenFileName(this,
        tr("Выбор прошивки"),
        LTR210_FPGA_FIRM_FILE,
        tr("Прошивка ПЛИС (*.rbf)"));
    if (!fileName.isEmpty())
    {
        int err = 0, res_err = 0;
        foreach (QLtr210Info *pinfo, m_devInfoList)
        {
            err = pinfo->device->loadFPGA(fileName);
            if (res_err==0)
                res_err = err;
        }

        if (res_err==LTR_OK)
        {
            QMessageBox::information(this, tr("Завершение загрузки"), tr("Прошивка загружена успешно!"));
        }
    }    
}

void OscMainWindow::on_actionRefreshDevList_triggered()
{
    saveSettings();
    getDevList();
}

void OscMainWindow::on_actionSetADC_triggered()
{
    setDevConfig();
}



void OscMainWindow::onColorChanged(int row, QColor color)
{
    foreach (LQGraphWgt* graph, m_graphs)
    {
        graph->setPlotColor(row, color);
    }
    foreach (LQGraphWgt* graph, m_graphs)
    {
        graph->graphUpdate();
    }
}

void OscMainWindow::onPlotEnChanged(int row, bool en)
{
    foreach (LQGraphWgt* graph, m_graphs)
    {
        graph->plotEnable(row, en);
    }
    foreach (LQGraphWgt* graph, m_graphs)
    {
        graph->graphUpdate();
    }
}

void OscMainWindow::onAdcSetChanged()
{
    if (ui->actionAutoupAdcCfg->isChecked() && run)
    {
        bool fnd = false;
        /* если это попытка изменить разрашение одного из каналов, то отменяем
         * ее, так как разрешенные каналы на лету менять нельзя */
        QGroupBox* group = qobject_cast<QGroupBox*>(sender());
        if (group)
        {
            foreach (QLtr210Info *info, m_devInfoList)
            {
                for (int ch=0; ch < LTR210_CHANNEL_CNT; ch++)
                {
                    if (group == info->ui.ch[ch].group)
                    {
                        group->setChecked(info->device->config().Ch[ch].Enabled);
                        fnd = true;
                    }
                }

                if (fnd)
                    break;
            }
        }

        if (!fnd)
        {
            setDevConfig();
        }
    }
}

void OscMainWindow::graphUpdate()
{
    m_graphUpdate = false;
    foreach (LQGraphWgt* graph, m_graphs)
    {
        if (graph->isVisible())
            graph->graphUpdate();
    }
}

void OscMainWindow::paramsChanged()
{
    int adc_div = ui->AdcFreqDiv->value();
    int adc_dcm = ui->AdcDcm->value();
    int frame_div = ui->FrameFreqDiv->value();

    double adc_f = (float)LTR210_ADC_FREQ_HZ/(adc_div*adc_dcm);
    double frame_f = (float)LTR210_FRAME_FREQ_HZ/frame_div;

    ui->adcFreq->setValue(adc_f);
    ui->frameFreq->setValue(frame_f);
}

void OscMainWindow::getDevList()
{
    devsClose();    

    qxtLog->info(tr("Поиск модулей..."));
    QList<QLtr210*> devlist = QLtr210::deviceList();
    QStringList sellist;



    /* Если больше одного устройства, то нужно выбрать, какие именно
     * модули будем использовать. Даем пользователю выбор, но перед этим
     * делаем начальный вариант на основании последних выбранных */
    if (devlist.size()>1)
    {
        /* получаем список устройств, которые были выбраны в последний раз
         * и остались выделенными */
        QSettings set;
        int cnt = set.beginReadArray("last_devs");
        for (int i=0; i < cnt; i++)
        {
            set.setArrayIndex(i);
            QString serial = set.value("serial").toString();
            for (int j=0; j < devlist.size(); j++)
            {
                if (serial == devlist[j]->serial())
                {
                    sellist.append(serial);
                }
            }
        }
        set.endArray();

        /* если не совпало ни одного серийного номера, то выбираем по умолчанию
         * первое устройство */
        if (sellist.size()==0)
            sellist.append(devlist[0]->serial());

        /* показываем диалог для выбора */
        DevSelectDialog dialog(devlist,  sellist, this);
        dialog.setWindowFlags(Qt::Dialog | Qt::MSWindowsFixedSizeDialogHint
                              | Qt::CustomizeWindowHint | Qt::WindowTitleHint);
        dialog.exec();

        /* оставляем только выбранные */
        devlist = dialog.selectedDevs();
    }
    else if (devlist.size()==0)
    {
        QMessageBox::warning(this, tr("Предупреждение"),
            tr("Не было найденно ни одного модуля LTR210.\nДля дальнейшей работы подключите нужный модуль и нажмите \"Обновить список устройтсв\""));
    }

    foreach (QLtr210* dev, devlist)
    {
        addDevice(dev);
    }

    if (m_devInfoList.isEmpty())
    {
        ui->actionStart->setEnabled(false);
        ui->actionLoadFPGA->setEnabled(false);
    }
    else
    {
        ui->actionStart->setEnabled(true);
        ui->actionLoadFPGA->setEnabled(true);
    }
}

void OscMainWindow::on_showCodes_toggled(bool checked)
{
    foreach (QLtr210Info *pinfo, m_devInfoList)
    {
        pinfo->device->setCodes(ui->showCodes->isChecked());
    }
}

void OscMainWindow::on_afcCorrect_toggled(bool checked)
{
    foreach (QLtr210Info *pinfo, m_devInfoList)
    {
        pinfo->device->setAfcCorrection(ui->afcCorrect->isChecked());
    }
}

void OscMainWindow::on_setAdcFreq_clicked()
{
    TLTR210_CONFIG cfg;
    memset(&cfg, 0, sizeof(cfg));
    double freq = ui->adcFreq->value();
    LTR210_FillAdcFreq(&cfg, freq, 0, NULL);
    ui->AdcFreqDiv->setValue(cfg.AdcFreqDiv+1);
    ui->AdcDcm->setValue(cfg.AdcDcmCnt+1);

    paramsChanged();

}

void OscMainWindow::on_setFrameFreq_clicked()
{
    TLTR210_CONFIG cfg;
    memset(&cfg, 0, sizeof(cfg));
    double freq = ui->frameFreq->value();
    LTR210_FillFrameFreq(&cfg, freq, NULL);
    ui->FrameFreqDiv->setValue(cfg.FrameFreqDiv+1);
    paramsChanged();
}
