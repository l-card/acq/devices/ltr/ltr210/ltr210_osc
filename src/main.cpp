#include <QtGui/QApplication>
#include "OscMainWindow.h"
#include <QTextCodec>

int main(int argc, char *argv[])
{
    QTextCodec::setCodecForCStrings(QTextCodec::codecForName("UTF-8"));
    QTextCodec::setCodecForTr(QTextCodec::codecForName("UTF-8"));

    QApplication a(argc, argv);

    a.setOrganizationName("L Card");
    a.setApplicationName("ltr210-osc");
    a.setOrganizationDomain("lcard.ru");
    a.setApplicationVersion(PRJ_VERION_STR);
    OscMainWindow w;

    w.show();

    w.getDevList();

    return a.exec();
}
