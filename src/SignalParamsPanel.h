#ifndef SIGNALPARAMSPANEL_H
#define SIGNALPARAMSPANEL_H

#include <QDockWidget>
#include "QLtr210.h"
#include <QList>

class LQTableWidget;
class QTableWidgetItem;


class SignalParamsPanel : public QDockWidget
{
    Q_OBJECT
public:
    explicit SignalParamsPanel(QWidget *parent = 0);

    
signals:
    
public slots:
    void setColors(QList<QColor> colors);
    void setChEnabled(QList<bool> en);
    /* добавление модуля, для которого нужно отображать параметры по двум каналам */
    void addDevice(QLtr210* dev);
    /* сброс счетчиков и показаний. выполняется перед стартом нового сбора */
    void resetParams();
    /* удаляем всю информацию для всех модулей */
    void clear();

    void showParams(QLtr210 *dev, int ch, double avg, double rms, double freq,
                    double snr, double thd, double sinad, double sfdr, double enob);
    void clearParams(QLtr210 *dev, int ch);

    void updateFrameCntr(QLtr210* dev, TLTR210_FRAME_STATUS status);

    LQTableWidget* table() {return m_tbl;}


private:
    static const int column_dev         = 0;
    static const int column_ch          = 1;
    static const int column_color       = 2;
    static const int column_par_first   = 3;
    static const int column_cntr_first  = 11;
    static const int column_overlap     = column_cntr_first+1;
    static const int column_skip_sync   = column_cntr_first+2;
    static const int column_inv_hist    = column_cntr_first+3;


    LQTableWidget* m_tbl;
    QStringList m_devNames;
    QList<QColor> m_colors;
    QList<bool> m_ch_en;
private slots:
    void on_itemDoubleClicked(QTableWidgetItem* item);
    void on_itemChanged(QTableWidgetItem* item);


signals:
    /* сигнал высылается при изменении цвета для отображения канала на графике */
    void colorChanged(int row, QColor color);
    /* сигнал вызывается каждый раз, когда изменяется настройка, показывать
     * определенный канал или нет */
    void plotEnChanged(int row, bool show);
};

#endif // SIGNALPARAMSPANEL_H
