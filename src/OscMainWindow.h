#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QProgressDialog>
#include <QThread>

#include "LQGraphWgt.h"
#include "QLtr210.h"
#include "SignalProcessing.h"



namespace Ui {
class OscMainWindow;
}

class QLedIndicator;
class SignalParamsPanel;
class QTreeWidgetItem;
class QCheckBox;
class QComboBox;
class QDoubleSpinBox;
class QGroupBox;
class LQxtTableLoggerEngine;

class OscMainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit OscMainWindow(QWidget *parent = 0);
    ~OscMainWindow();
public slots:
    void getDevList();
protected:
    void closeEvent ( QCloseEvent * event );
    
private slots:
    void on_actionStart_triggered();
    void onFrameRecv(QLtr210* dev, LQChFrameParams ch1, LQChFrameParams ch2, TLTR210_FRAME_STATUS status);
    void onError(QString errTitle, int err);

    void onLoadFpgaStart(quint32 size);
    void onLoadFpgaProgr(quint32 done_size, quint32 total_size);
    void onLoadFpgaDone(qint32 err);

    int setDevConfig();
    void configUiEnable(bool en);

    void addDevice(QLtr210 *dev);


    void on_actionStop_triggered();
    void on_actionFrameStart_triggered();
    void on_actionLoadFPGA_triggered();
    void on_actionRefreshDevList_triggered();
    void on_actionSetADC_triggered();
    void on_showCodes_toggled(bool checked);

    void onColorChanged(int row, QColor color);
    void onPlotEnChanged(int row, bool en);
    void onAdcSetChanged();

    void graphUpdate();
    void paramsChanged();



    void on_afcCorrect_toggled(bool checked);

    void on_setAdcFreq_clicked();

    void on_setFrameFreq_clicked();

private:
    struct QLtr210Info
    {
        QLtr210* device;

        struct
        {
            QTreeWidgetItem *devItem;
            QTreeWidgetItem *cfgItem;
            QLedIndicator* runLed;
            QLedIndicator* progLed;
            QLedIndicator* errLed;
            struct
            {
                QGroupBox* group;
                QComboBox* range;
                QComboBox* mode;
                QComboBox* dataBitMode;
                QDoubleSpinBox* syncLvlL;
                QDoubleSpinBox* syncLvlH;
            } ch[LTR210_CHANNEL_CNT];
            QComboBox* syncMode;
            QComboBox* groupMode;
        } ui;
    } ;


    Ui::OscMainWindow *ui;

    QLedIndicator*  m_progLed;
    bool m_graphUpdate;

    LQGraphWgt *m_timeGraph, *m_fftGraph, *m_dinGraph;
#ifdef LTR210_OSC_ENABLE_HIST
    LQGraphWgt *m_histGraph;
#endif
    QList<LQGraphWgt*> m_graphs;
    QList<QLtr210Info*> m_devInfoList;
    bool run;

    bool ledBlink; /* признак, что сейчас идет мигание светодиодом и не нужно
                      повторно запускать */

    void saveSettings();
    void clearPlot(int plot_id);
    void showPlot(QLtr210 *dev, int plot_id, int id, LQChFrameParams *ch_par);
    void devsClose();


    QProgressDialog* m_progrDialog;
    SignalParamsPanel *m_paramPanel;

    LQxtTableLoggerEngine* m_tblLog;

    SignalProcessing m_sigProc;
    QThread m_sigProcThread;
};



#endif // MAINWINDOW_H
