#ifndef QLTR210_H
#define QLTR210_H

#include <QObject>
#include <QThread>
#include <QList>
#include <QxtLogger>


#include "ltr/include/ltr210api.h"


/* параметры принятого сигнала */
struct LQChFrameParams
{
    int ch;
    int size; /* кол-во данных */
    int fft_size;
    double* data; /* данные */
    double* din; /* состояние бита синхронизации во входном потоке (массив 1 или 0) */
    double* fft; /* спектр */
    double df;
    double dt;
    double range_val;
    double f_max;
    double avg; /* среднее значение */
    double rms; /* СКЗ сигнала */
    double freq; /* частота, соответствующая макс. гармонике */
    double snr; /* сигнал-шум */
    double thd;
    double sinad; /* сигнал/(шум + гармоники) */
    double sfdr;
    double enob; /* кол-во эффективных бит */
};

static double f_range_vals[] = {10.,5.,2.,1.,0.5};


class QLtr210 : public QThread
{
    Q_OBJECT
public:

    static QList<QLtr210*> deviceList();
    static QString errorString(int err);



    explicit QLtr210(int slot, QString csn, QString ctype, QObject *parent = 0);
    int open();
    int close();
    int setConfig(const TLTR210_CONFIG* cfg);
    int loadFPGA(QString filename);

    void stopRequest();
    int startFrame();
    int startADC();
    int measZero();



    TLTR210_CONFIG config() const {return m_hnd.Cfg;}
    double adcFreq() const {return m_hnd.State.AdcFreq;}

    QString serial() const {return QString::fromAscii(m_hnd.ModuleInfo.Serial);}
    QString crateStr() const {return m_crate_type + " (" + m_csn + ")";}
    int slot() const {return m_slot;}
    QString devStr() const {return serial() + ", Слот " + QString::number(m_slot);}
    QString devDescr() const;

    void setCodes(bool codes) {m_codes = codes;}
    void setZeroCorrection(bool en) {m_zero_cor = en;}
    void setAfcCorrection(bool correction) {m_afc_cor = correction;}


    void run();
    
signals:
    void frameRecvd(LQChFrameParams ch1, LQChFrameParams ch2, TLTR210_FRAME_STATUS status, int proc_flags);
    void error(QString errTitle, int err) const;
    void loadFpgaStart(quint32 size);
    void loadFpgaProgr(quint32 done, quint32 size);
    void loadFpgaStop(qint32 err);
    void devDescrChanged(QString descr);
public slots:

    void reportError(QString message, int err) const;
    void reportMessage(QxtLogger::LogLevel lvl, QString message) const;



private:

    static void APIENTRY cb_load_porgr(void* cb, TLTR210 *hnd, DWORD done_cnt, DWORD total_cnt);
    void sendFpgaStart(quint32 size);
    void sendFpgaProgr(quint32 done, quint32 size);
    int processRecvdFrame(DWORD *wrds, double *data, TLTR210_DATA_INFO *info, int size, int proc_flags, TLTR210_FRAME_STATUS status);

    mutable TLTR210 m_hnd;
    bool m_stop_req;
    bool m_open;
    bool m_codes;
    bool m_zero_cor;
    bool m_afc_cor;
    bool m_fpga_is_loaded;
    int m_slot;
    QString m_csn;
    QString m_crate_type;

    
};

#endif // QLTR210_H
