#include "DevSelectDialog.h"
#include <QTableWidget>
#include <QVBoxLayout>
#include <QStringList>
#include <QDialogButtonBox>
#include <QHeaderView>
#include <QSettings>




DevSelectDialog::DevSelectDialog(QList<QLtr210*> devlist, QStringList sellist, QWidget *parent) :
    QDialog(parent), m_devlist(devlist)
{
    QVBoxLayout* lout = new QVBoxLayout();
    this->setWindowTitle(tr("Выбор используемых модулей"));
    this->setSizeGripEnabled(true);

    QStringList columns;
    columns << tr("Модуль") << tr("Серийный номер") << tr("Слот") << tr("Крейт");
    m_devTbl = new QTableWidget(devlist.size(), columns.size());

    m_devTbl->setEditTriggers(QAbstractItemView::NoEditTriggers);
    m_devTbl->setHorizontalHeaderLabels(columns);
    m_devTbl->verticalHeader()->setVisible(false);
    m_devTbl->horizontalHeader()->setStretchLastSection(true);
    m_devTbl->setSelectionBehavior(QAbstractItemView::SelectRows);
    m_devTbl->setSelectionMode(QAbstractItemView::SingleSelection);
    m_devTbl->horizontalHeader()->setSelectionMode(QAbstractItemView::NoSelection);
    m_devTbl->horizontalHeader()->setClickable(false);
    m_devTbl->setSelectionMode(QAbstractItemView::NoSelection);
    m_devTbl->setCurrentItem(0);

    int row = 0;
    foreach (QLtr210* dev, devlist)
    {
        QTableWidgetItem *devitem = new QTableWidgetItem("LTR210");
        devitem->setFlags(devitem->flags() | Qt::ItemIsUserCheckable);
        m_devTbl->setItem(row, 0, devitem);
        m_devTbl->setItem(row, 1, new QTableWidgetItem(dev->serial()));

        m_devTbl->setItem(row, 2, new QTableWidgetItem(QString::number(dev->slot())));
        m_devTbl->setItem(row, 3, new QTableWidgetItem(dev->crateStr()));


        int fnd,j;
        for (j=0, fnd=0; !fnd && (j < sellist.size()); j++)
        {
            if (sellist[j] == dev->serial())
            {
                fnd = 1;
            }
        }

        devitem->setCheckState(fnd ? Qt::Checked : Qt::Unchecked);        
        row++;
    }

    m_devTbl->resizeColumnsToContents();
    m_devTbl->resizeRowsToContents();

    lout->addWidget(m_devTbl);


    QDialogButtonBox* m_btnBox;

    m_btnBox = new QDialogButtonBox(QDialogButtonBox::Ok);
    connect(m_btnBox, SIGNAL(accepted()), this, SLOT(accept()));
    connect(m_btnBox, SIGNAL(rejected()), this, SLOT(reject()));
    lout->addWidget(m_btnBox);
    this->setLayout(lout);



    QSettings set;
    set.beginGroup("DevSelectDialog");
    resize(set.value("size", QSize(500, 200)).toSize());
    if ((set.value("devTableCoumnState").toByteArray().size()!=0))
    {
        m_devTbl->horizontalHeader()->restoreState(set.value("devTableCoumnState").toByteArray());
    }
    set.endGroup();
}

QSize DevSelectDialog::sizeHint() const
{
    return QSize(500, 300);
}

QList<QLtr210 *> DevSelectDialog::selectedDevs()
{
    QList<QLtr210 *> sellits;
    for (int row=0; row < m_devlist.size(); row++)
    {
        if (m_devTbl->item(row, 0)->checkState()==Qt::Checked)
        {
            sellits.append(m_devlist.at(row));
        }
    }

    return sellits;
}

void DevSelectDialog::done(int r)
{
    QSettings set;
    set.beginGroup("DevSelectDialog");
    set.setValue("size", size());
    set.setValue("devTableCoumnState", m_devTbl->horizontalHeader()->saveState());
    set.endGroup();
    QDialog::done(r);
}
