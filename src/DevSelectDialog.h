#ifndef LTESTDEVSELECTDIALOG_H
#define LTESTDEVSELECTDIALOG_H

#include <QDialog>
#include <QStringList>
#include "QLtr210.h"

class QTableWidget;



class DevSelectDialog : public QDialog
{
    Q_OBJECT
public:
    explicit DevSelectDialog(QList<QLtr210*> devlist, QStringList sellist, QWidget *parent = 0);
    
    virtual QSize sizeHint () const;

    QList<QLtr210*> selectedDevs();    

public slots:
    virtual void done ( int r );
private:
    QList<QLtr210 *> m_devlist;
    QTableWidget* m_devTbl;
};

#endif // LTESTDEVSELECTDIALOG_H
